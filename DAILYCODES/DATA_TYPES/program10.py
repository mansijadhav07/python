#List

progLang=["C","CPP","Java","Python"]
listData=[18,"Virat","MSD",4.5,12]

print(progLang)
print(listData)
print(type(progLang))

'''
OUTPUT
['C', 'CPP', 'Java', 'Python']
[18, 'Virat', 'MSD', 4.5, 12]
<class 'list'>
'''
