data1=True
data2=False
ans=data1+data2
print(ans)#1
'''
In Python, when you perform arithmetic operations on Boolean values, 
True is treated as 1, and False is treated as 0. 
Therefore, when you add data1 (True, which is 1) 
and data2 (False, which is 0), the result is 1.
'''
