#Boolean Datatype

data1=True
data2=False
print(data1)
print(data2)
print(type(data1))
print(type(data2))

'''
OUTPUT
True
False
<class 'bool'>
<class 'bool'>
'''
