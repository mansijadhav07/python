listData=[10,20,30,40,50]
byteData=bytearray(listData)
print(listData)
for i in byteData:
    print(i)

print("After updating")
byteData[2]=200
for i in byteData:
    print(i)

'''
[10, 20, 30, 40, 50]
10
20
30
40
50
After updating
10
20
200
40
50

bytearray is mutable
'''

