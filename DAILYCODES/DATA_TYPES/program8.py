#Sequence Type
#String

#four ways to write string

#Type 1
friend1="Kanha"
print(friend1)
print(type(friend1))

#Type 2
friend2='Rahul'
print(friend2)
print(type(friend2))

#Type 3
friend3='''Mahesh'''
print(friend3)
print(type(friend3))

#Type 4
friend4=" " "Nisha" " "
print(friend4)
print(type(friend4))

'''
Kanha
<class 'str'>
Rahul
<class 'str'>
Mahesh
<class 'str'>
 Nisha
<class 'str'>
'''
