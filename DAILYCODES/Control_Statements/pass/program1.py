age=int(input("Enter age:"))
if(age>=18):
    pass
print("Eligible")
print("out of if")
'''
Enter age:34
Eligible
out of if

When the user does not know what code to write,
So user simply places a pass at that line.
Sometimes, the pass is used when the user 
doesn’t want any code to execute. So users 
can simply place a pass where empty code is not allowed, 
like in loops, function definitions, class definitions, 
or in if statements. So using a pass statement user avoids this error
'''
