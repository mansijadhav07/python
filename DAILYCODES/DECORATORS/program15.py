#decorator chaining

def decorFun(func):
    def wrapper():
        print("Start wrapper fun")
        func()
        print("End wrapper fun")
    return wrapper

def decorRun(func):
    def wrapper():
        print("Start wrapper run")
        func()
        print("End wrapper run")
    return wrapper

@decorFun
@decorRun
def normalFun():
    print("In normal fun")

normalFun()
'''
Start wrapper fun
Start wrapper run
In normal fun
End wrapper run
End wrapper fun
'''
