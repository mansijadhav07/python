def decorFun(func):
    def wrapper():
        print("Start wrapper")
        func()
        print("End wrapper")
    return wrapper

@decorFun
def normalFun():
    print("In normal fun")

normalFun()
'''
Start wrapper
In normal fun
End wrapper
'''
