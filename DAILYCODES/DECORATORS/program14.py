def decorFun(func):
    print("In decorfun")
    def wrapper(*args):
        print("Start wrapper")
        val=func(*args)
        print("End wrapper")
        return val
    return wrapper

@decorFun
def normalFun(x,y):
    print("In normal fun")
    return x+y

print(normalFun(10,20))
'''
In decorfun
Start wrapper
In normal fun
End wrapper
30
'''
