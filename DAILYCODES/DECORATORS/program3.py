def outerFunc():
    print("In outer function")

    def innerFunc1():
        print("In inner function 1")
    def innerFunc2():
        print("In inner function 2")

    return innerFunc1,innerFunc2

ret=outerFunc()
for x in ret:
    x()

'''
In outer function
In inner function 1
In inner function 2
'''
