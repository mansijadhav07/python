def decorFunc(func):
    def wrapper():
        print("Start wrapper")
        func()
        print("End wrapper")
    return wrapper

def normalFunc():
    print("In normal function")

ret=decorFunc(normalFunc)
ret()
'''
Start wrapper
In normal function
End wrapper
'''

