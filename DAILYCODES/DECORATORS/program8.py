def decorFun(func):
    def wrapper():
        print("Start wrapper")
        func()
        print("End wrapper")
    return wrapper

def normalFun():
    print("Hello I am in normal fun")

normalFun=decorFun(normalFun)
normalFun()

'''
Start wrapper
Hello I am in normal fun
End wrapper
'''
