def decorFun(func):
    print("In decorfun")
    def wrapper():
        print("Start wrapper")
        func()
        print("End wrapper")
    return wrapper

def normalFun(x,y):
    print("In normal fun")
    return x+y


print(decorFun(normalFun(10,20)))
'''
In normal fun
In decorfun
<function decorFun.<locals>.wrapper at 0x7f8b312c9f70>
'''
