class Instagram:
    def __init__(self):
        print("in constructor")
    @staticmethod
    def reels():
        print("In static method")
    def story(self):
        print("In instance method")
    @classmethod
    def profile(self):
        print("In class method")
if __name__=="__main__":
    obj=Instagram()
    Instagram.profile()
    obj.story()
    Instagram.reels()
