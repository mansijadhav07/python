class Demo:
    def __init__(self):
        print("demo constructor")
    def __del__(self):
        print("demo destructor")
if __name__=="__main__":
    obj1=Demo()
    obj2=Demo()
    obj3=obj1
    obj4=Demo()
    del obj2
