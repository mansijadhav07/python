class Parent:
    def __init__(self):
        print("In parent constructor")
    
    def work(self):
        print("In parent instance method")

    @staticmethod
    def enjoy():
        print("In parent static method")
      
    @classmethod
    def sleep(self):
        print("In parent class method")

class Child(Parent):
    pass
if __name__=="__main__":
    obj=Child()
    obj.work()
    Child.enjoy()
    Child.sleep()
