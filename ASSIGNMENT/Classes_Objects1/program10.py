class Google:
    def __init__(self):
        print("In Google constructor")
    def info(self):
        print("Google is a MAANG company")
class Bard(Google):
    def __init__(self):
        print("In Bard constructor")
    def info(self):
        print("Bard is an AI chatbot")

if __name__=="__main__":
    obj1=Bard()
    obj1.info()
    obj2=Google()
    obj2.info()

