class Vehicle:
    def __init__(self,brand,model,year,speed):
        print("In Vehicle constructor")
        self.brand=brand
        self.model=model
        self.year=year
        self.speed=speed
    def accelerate(self):
        print("In vehicle accelerate")
    def brake(self):
        print("In vehicle brake")
    def honk(self):
        print("In vehicle honk")

class Child(Vehicle):
    def __init__(self,brand,model,year,speed):
        print("In Child constructor")
        self.brand=brand
        self.model=model
        self.year=year
        self.speed=speed
    
    def accelerate(self):
        print("In child accelerate")
    def honk(self):
        print("In child honk")


brand=input("Enter brand name:")
model=input("Enter model name:")
year=int(input("Enter year :"))
speed=int(input("Enter speed: "))
if __name__=="__main__":
    obj1=Vehicle(brand,model,year,speed)
    obj2=Child(brand,model,year,speed)
    obj2.accelerate()
    obj2.honk()
