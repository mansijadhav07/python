class Human:
    def __init__(self,name,age):
        print("In demo constructor")
        self.name=name
        self.age=age
    def information(self):
        print("Name is:",self.name)
        print("age is:",self.age)
name=input("Enter name:")
age=int(input("Enter age:"))

if __name__=="__main__":
    obj1=Human(name,age)
    obj1.information()
'''
Enter name:tejas
Enter age:32
In demo constructor
Name is: tejas
age is: 32
'''
